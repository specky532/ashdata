from openpyxl import Workbook, load_workbook
from datetime import datetime

class CompareData:
	def __init__(self, oldList, newList, fileLoc):
		self.oldData = oldList

		self.newData = newList

		self.groupsStillThere = []

		self.fileLoc = fileLoc + '.xlsx'

		self.companiesChanged = []

		self.writeBuffer = []


	def RunComparison(self):
		#ALways compare backwards and forwards, assuming one could be added and one removed

		if self.oldData == self.newData:
			print "No Changes Found"

		else:
			for oldPoint in self.oldData: #Check for anything deleted
				#Check if it still exists
				point = self.SearchArrayBusName(oldPoint.businessEntityName, self.newData)

				if point != -1:
					#Group still exists, compare everything
					comparePoint = self.newData[point]

					#ABN
					if oldPoint.ABN != comparePoint.ABN:
						self.AddToWriteBuffer(oldPoint, "Changed its abn to: " + comparePoint.ABN)

					#tradingName
					if oldPoint.tradingName != comparePoint.tradingName:
						self.AddToWriteBuffer(oldPoint, "Changed its trading name to: " + comparePoint.tradingName)

					#All employees
					if oldPoint.NumberOfWorkers() != comparePoint.NumberOfWorkers():
						self.AddToWriteBuffer(oldPoint, "Changed its number of employees from: " + str(oldPoint.NumberOfWorkers()) + " To: " + str(comparePoint.NumberOfWorkers()))
					#num Eployees, still check though
					for emp in oldPoint.allEmployees:
						empIndex = self.SearchArrayEMP(emp.lobName, comparePoint.allEmployees)

						if empIndex != -1: #Employee Still Exists, Check for changes
							empComparePoint = comparePoint.allEmployees[empIndex]

							#ID Num
							if emp.idNum != empComparePoint.idNum:
								self.AddToWriteBuffer(oldPoint, "Employee: " + emp.lobName + " Changed their ID number from: " + emp.idNum + " To: " + empComparePoint.idNum)

							#lobPosition
							if emp.lobPosition != empComparePoint.lobPosition:
								self.AddToWriteBuffer(oldPoint, "Employee: " + emp.lobName + " Changed their position from: " + emp.lobPosition + " To: " + empComparePoint.lobPosition)

							#wasGovRep
							if emp.wasGovRep != empComparePoint.wasGovRep:
								self.AddToWriteBuffer(oldPoint, "Employee: " + emp.lobName + " Changed their gov Rep Status from: " + emp.wasGovRep + " To: " + empComparePoint.wasGovRep)
								
							#cesationDate
							if emp.cesationDate != empComparePoint.cesationDate:
								self.AddToWriteBuffer(oldPoint ,"Employee: " + emp.lobName + " Changed their cesation date from: " + emp.cesationDate + " To: " + empComparePoint.cesationDate)

						else:
							#Employee Deleted
							self.AddToWriteBuffer(oldPoint, emp.lobName + " Was removed from the list of employees (A Spelling Change may have also occured, check other changes for this date)")

					#Check for employees added
					for emp in comparePoint.allEmployees:
						empIndex = self.SearchArrayEMP(emp.lobName, oldPoint.allEmployees)

						if empIndex == -1: #Employee added 
							self.AddToWriteBuffer(oldPoint, "Added a new Employee with details " + emp.FormatData())


					#All Clients
					if oldPoint.NumberOfClients() != comparePoint.NumberOfClients():
						self.AddToWriteBuffer(oldPoint, "Changed its number of clients from: " + str(oldPoint.NumberOfClients()) + " To: " + str(comparePoint.NumberOfClients()))
					
					#Check for deletions
					for cli in oldPoint.allClients:
						cliIndex = self.SearchArrayCLI(cli.clientName, comparePoint.allClients)

						if cliIndex == -1: #Client Deleted
							self.AddToWriteBuffer(oldPoint, cli.clientName + " Was removed from the list of clients (A Spelling Change may have also occured, check other changes for this date)")

					# Check for additions
					for cli in comparePoint.allClients:
						cliIndex = self.SearchArrayCLI(cli.clientName, oldPoint.allClients)

						if cliIndex == -1: #Client Added
							self.AddToWriteBuffer(oldPoint, " Added a new Client with details " + cli.FormatData())


					#All owners
					if oldPoint.NumberOfOwners() != comparePoint.NumberOfOwners():
						self.AddToWriteBuffer(oldPoint, "Changed its number of owners from: " + str(oldPoint.NumberOfOwners()) + " To: " + str(comparePoint.NumberOfOwners()))
					
					#Check for deletions
					for own in oldPoint.owners:
						ownIndex = self.SearchArrayOWN(own, comparePoint.owners)

						if ownIndex == -1: #Employee Deleted
							self.AddToWriteBuffer(oldPoint, own + " Was removed from the list of owners (A Spelling Change may have also occured, check other changes for this date)")

					# Check for additions
					for own in comparePoint.owners:
						ownIndex = self.SearchArrayOWN(own, oldPoint.owners)

						if cliIndex == -1: #Employee Added
							self.AddToWriteBuffer(oldPoint, "Added a new owner with name " + own)


				else:
					# Group Deleted
					self.AddToWriteBuffer(oldPoint, "ABN:" + oldPoint.ABN + " Was deleted or changed it's businessname, Check additions for today")
				

			# Check for new points added	
			for newPoint in self.newData: #Check for anything added
				#Check if it still exists
				point = self.SearchArrayBusName(newPoint.businessEntityName, self.oldData)

				if point == -1: # Not found so added
					self.AddToWriteBuffer(newPoint, "A New lobby group was registed with details: " + newPoint.FormatData())

		self.SaveChange()
		self.UpdateExcelData()

	def AddToWriteBuffer(self, company, change):
		temp = [company, change]
		self.writeBuffer.append(temp)
		



	def SearchArrayOWN(self, lookingFor, dataToSearch):
		for i in range(len(dataToSearch)):
			if lookingFor == dataToSearch[i]:
				return i

		return -1


	def SearchArrayCLI(self, lookingFor, dataToSearch):
		for i in range(len(dataToSearch)):
			if lookingFor == dataToSearch[i].clientName:
				return i

		return -1



	def SearchArrayEMP(self, lookingFor, dataToSearch):
		for i in range(len(dataToSearch)):
			if lookingFor == dataToSearch[i].lobName:
				return i

		return -1



	def SearchArrayBusName(self, lookingFor, data):
		for i in range(len(data)):
			if lookingFor == data[i].businessEntityName:
				return i
		return -1


	def FormatDataForExcel(self, data):
		data = data.encode('ascii', 'ignore').decode('ascii') #Remove Weird Formatting
		data = data.replace(',', ' ')

		return data



	def SaveChange(self):
		#Date, #Company, #Change, #Company's Full Details
		wb = load_workbook(filename = self.fileLoc)

		# grab the active worksheet
		#ws = wb.active
		ws = wb['changes']

		time = datetime.strftime(datetime.now(), '%Y-%m-%d')

		for bufferedChange in self.writeBuffer:
			# Data can be assigned directly to cells
			companyName = self.FormatDataForExcel(bufferedChange[0].businessEntityName)
			abn = self.FormatDataForExcel(bufferedChange[0].ABN)
			change = self.FormatDataForExcel(bufferedChange[1])

			# Append Change
			ws.append([time, companyName, abn, change])

			print 
			print time + " Change Found: " + companyName + " " + change

			
			#Add to list of companies changed
			pos = self.SearchArrayBusName(bufferedChange[0].businessEntityName, self.newData) #Check if deleted so add new one

			if pos == -1: #Was deleted, save last copy
				self.companiesChanged.append(bufferedChange[0])
			else: 
				pos2 = self.SearchArrayBusName(bufferedChange[0].businessEntityName, self.companiesChanged) #Find position and update with new data

				if pos2 == -1: #Hasn't been added yet
					updatedData = self.newData[pos]
					self.companiesChanged.append(updatedData)
		print
		print str(len(self.writeBuffer)) + " Changes Found and saved"
		print

		# Save the file
		wb.save(self.fileLoc)

		




	def UpdateExcelData(self):
		for group in self.companiesChanged:
			group.SaveToExcel()

		print str(len(self.companiesChanged)) + " Companies Excel Updated /n"





