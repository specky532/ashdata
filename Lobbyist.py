from bs4 import BeautifulSoup

class Lobbyist:

	def __init__(self, col):

		self.idNum = col[0].get_text()
		self.lobName = col[1].get_text()
		self.lobPosition = col[2].get_text()
		self.wasGovRep = col[3].get_text()
		cesationDate = col[4].get_text()

		if len(cesationDate) == 0:
			self.cesationDate = "Not Ceased"
		else:
			self.cesationDate = cesationDate





	def FormatData(self):
		return 	("ID: " + self.idNum + 
				"\nLobbyist Name: " + self.lobName + 
				"\nPosition: " + self.lobPosition + 
				"\nFormer Gov Rep: " + self.wasGovRep + 
				"\nCeasation Date: " + self.cesationDate)

