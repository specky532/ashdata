from bs4 import BeautifulSoup
import urllib
from Lobbyist import Lobbyist
from LobbyGroup import LobbyGroup
from datetime import datetime
from CompareData import CompareData
import pickle


allLobbyGroups = []

def DownloadPage(urlToDL):

	print "Downloading URL"
	url = urllib.urlopen(urlToDL)
	#fileName = datetime.strftime(datetime.now(), '%Y-%m-%d') + ".xls"
	print "URL Downloaded"
	return url


def ConvertToSoup(url):
	print "Converting To Soup"
	soup = BeautifulSoup(url, 'lxml')
	print "Converted to Soup"

	return soup


def ProcessPage(soupPage):
	#Assuming one table
	table = soupPage.find_all('table')[0] #Extract the first table
	rows = table.find_all('tr')[1:] #Skip the first row

	print str(len(rows)) + " Results found"

	count = 1

	toDL = len(rows)

	for row in rows:
		print "Processing " + str(count) + " of " + str(len(rows))

		col = row.find_all('td')
		url = col[2].find('a')['href']

		lobbyObject = LobbyGroup(url)
		allLobbyGroups.append(lobbyObject)

		count += 1

	print "All Groups imported"


def SaveDataTextFile():
	#Save two copies
	fileName = datetime.strftime(datetime.now(), '%Y-%m-%d') + ".txt"
	archive = open('archiveData/'+fileName, 'wb')

	#Delete contents
	open("recent.txt","w").close()
	#Start again
	recent = open('recent.txt', 'wb')

	count = 1

	#One in archive one in most recent
	for group in allLobbyGroups:
		data = group.FormatData()
		data = data.encode('ascii', 'ignore').decode('ascii') #Remove Weird Formatting


		recent.write(data)
		archive.write(data)

		print "Saved " + str(count) + " of " + str(len(allLobbyGroups))
		count += 1



	recent.close()
	archive.close()

def SaveDataPickle():
	fileName = datetime.strftime(datetime.now(), '%Y-%m-%d') + "PIK.txt"

	#Delete most recent
	open("recentPIK.txt","w").close()

	# Save in recent
	with open("recentPIK.txt", "wb") as f:
		pickle.dump(len(allLobbyGroups), f)
		for group in allLobbyGroups:
			pickle.dump(group, f)

	# Save Archive
	with open('archiveData/' + fileName, "wb") as f:
		pickle.dump(len(allLobbyGroups), f)
		for group in allLobbyGroups:
			pickle.dump(group, f)

	print "Saved Classes"



def LoadDataPickle(fileName):
	allData = []

	with open(fileName, "rb") as f:
		for _ in range(pickle.load(f)):
			allData.append(pickle.load(f))

	print "Loaded Classes"
	return allData


def SaveDataEXCELFile():
	count = 1
	for group in allLobbyGroups:
		group.SaveToExcel()

		print "Saved To Excel " + str(count) + " of " + str(len(allLobbyGroups))
		count += 1





url = DownloadPage('http://lobbyists.pmc.gov.au/who_register.cfm')
soup = ConvertToSoup(url)
ProcessPage(soup)
SaveDataTextFile()

#SaveDataEXCELFile() #Only run the first time

print


lastData = LoadDataPickle("recentPIK.txt")

#Compare files
comp = CompareData(lastData, allLobbyGroups, 'listOfChangesByDate')
comp.RunComparison()

print "Compared Classes"
print "Saved Changes"

SaveDataPickle()




print 




