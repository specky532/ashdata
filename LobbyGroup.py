from Lobbyist import Lobbyist
from Clients import Clients
from bs4 import BeautifulSoup
import urllib
from openpyxl import Workbook, load_workbook
from datetime import datetime



class LobbyGroup:

	def __init__(self, detailURL):

		page = self.ConvertToSoup(self.DownloadPage('http://lobbyists.pmc.gov.au/' + detailURL))

		# First DIV

		profileDiv = page.find("div", {"id": "profile"})
		htmlLists = profileDiv.find_all('ul')

		entityInfo = htmlLists[0].find_all('li')


		self.businessEntityName = entityInfo[0].get_text()[22:]
		self.ABN = entityInfo[1].get_text()[7:]
		self.tradingName = entityInfo[2].get_text()[14:]


		#First Table, second DIV
		table = page.find_all('table')[0] #Extract the first table
		rows = table.find_all('tr')[1:] #Skip the first row
		self.allEmployees = []

		didFuckUp = False

		try:
			for row in rows:
				col = row.find_all('td')
				lobbyObject = Lobbyist(col)
				self.allEmployees.append(lobbyObject)
		except Exception as e:
			self.allEmployees = []
			didFuckUp = True

		noClientsFuckUp = False

		#Second table, third DIV
		if didFuckUp:
			table = page.find_all('table')[0] #Extract the first table
		else:
			try:
				table = page.find_all('table')[1] #Extract the second table
			except Exception as e:
				noClientsFuckUp = True

		
		rows = table.find_all('tr')[1:] #Skip the first row
		self.allClients = []

		for row in rows:
			try:
				col = row.find_all('td')
				clientObject = Clients(col)
				self.allClients.append(clientObject)
			except Exception as e:
				self.allClients = []

		# Fourth detail
		self.owners = []
		ownerTags = htmlLists[1].find_all('li')
		for o in ownerTags:
			self.owners.append(o.get_text())



	def DownloadPage(self, urlToDL):
		return urllib.urlopen(urlToDL)


	def ConvertToSoup(self, url):
		return BeautifulSoup(url, 'lxml')


	def NumberOfClients(self):
		if len(self.allClients) == 0:
			return "No Clients Listed"
		else:
			return len(self.allClients)

	def FormatClients(self):
		return self.FormatList(self.allClients)


	def FormatWorkers(self):
		return self.FormatList(self.allEmployees)

	def FormatList(self, objectToForm):
		string = ''

		for l in objectToForm:
			string += l.FormatData() + '\n'

		return string

	def NumberOfWorkers(self):
		if len(self.allEmployees) == 0:
			return "No Employees Listed"
		else:
			return len(self.allEmployees)

	def NumberOfOwners(self):
		if len(self.owners) == 0:
			return "No Owners Listed"
		else:
			return len(self.owners)


	def FormatOwners(self):
		string = ''

		for owner in self.owners:
			string += owner + '\n'

		return string


	def FormatData(self):
		string = "Business Name: " + self.businessEntityName + " 		Trading Name: " + self.tradingName + " 		ABN: " + self.ABN + "\n"
		string += "Owners: {0}\n".format(len(self.owners))
		string += self.FormatOwners() + "\n"
		string += "Employees: {0}\n".format(self.NumberOfWorkers())
		string += self.FormatWorkers() + "\n"
		string += "Clients: {0}\n".format(self.NumberOfClients())
		string += self.FormatClients() + "\n\n\n"

		return string


	def SaveToExcel(self):
		wb = load_workbook(filename = 'listOfChangesByDate.xlsx')

		#Check if sheet already exists


		name = self.businessEntityName[:25]
		name = name.encode('ascii', 'ignore').decode('ascii') #Remove Weird Formatting
		name = name.replace(',', ' ')
		name = name.replace('\\', ' ')
		name = name.replace('/', ' ')
		name = name.replace('*', ' ')
		name = name.replace('[', ' ')
		name = name.replace(']', ' ')
		name = name.replace(':', ' ')
		name = name.replace('?', ' ')


		try:
			ws = wb[name]
			wb.remove_sheet(ws)
		except Exception as e:
			pass

		ws = wb.create_sheet(title=name)
		ws.page_setup.fitToWidth = 1

		ws['A1'] = 'Business Name'
		ws['A2'] = 'Trading Name'
		ws['D1'] = 'ABN'
		ws['D2'] = 'Last Updated'
		ws['A5'] = 'Number of Employees'
		ws['H5'] = 'Number of Clients'
		ws['L5'] = 'Number of Owners'


		# Set Titles
		ws['B1'] = self.businessEntityName
		ws['B2'] = self.tradingName
		ws['E1'] = self.ABN
		ws['E2'] = datetime.strftime(datetime.now(), '%Y-%m-%d')
		ws['B5'] = self.NumberOfWorkers()
		ws['I5'] = self.NumberOfClients()
		ws['M5'] = self.NumberOfOwners()


		#Update Employees
		startI = 6
		for emp in self.allEmployees:
			ws.cell(row = startI, column = 1).value = emp.idNum
			ws.cell(row = startI, column = 2).value = emp.lobName
			ws.cell(row = startI, column = 3).value = emp.lobPosition
			ws.cell(row = startI, column = 4).value = emp.wasGovRep
			ws.cell(row = startI, column = 5).value = emp.cesationDate
			startI += 1

		startI = 6
		for cli in self.allClients:
			ws.cell(row = startI, column = 8).value = cli.idNum
			ws.cell(row = startI, column = 9).value = cli.clientName
			startI += 1


		startI = 6
		for own in self.owners:
			ws.cell(row = startI, column = 12).value = own
			startI += 1

		# Save the file
		wb.save('listOfChangesByDate.xlsx')
